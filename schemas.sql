

CREATE TABLE IF NOT EXISTS tbl_raw_input_data(
    pk_id INTEGER PRIMARY KEY AUTOINCREMENT,
    transaction_id VARCHAR(50),
    rundate VARCHAR(50),
    age INTEGER,
	sex VARCHAR(50),
	job INTEGER,
	housing VARCHAR(50),
	saving_accounts VARCHAR(50),
	checking_account VARCHAR(50),
	credit_amount INTEGER,
	duration INTEGER,
	purpose VARCHAR(50)
);

-- COLOCAR NOT NULL -- AGREGAR ÍNDICES


CREATE TABLE IF NOT EXISTS tbl_encoding_data(
	pk_id INTEGER NOT NULL UNIQUE,
	transaction_id VARCHAR(50) NOT NULL,
	age INTEGER,
	Age_cat_Young INTEGER,
	Age_cat_Adult INTEGER,
	Age_cat_Senior INTEGER,
	Sex_male INTEGER,
	job INTEGER,
	Housing_own INTEGER,
	Housing_rent INTEGER,
	Savings_moderate INTEGER,
	Savings_no_inf INTEGER,
	"Savings_quite rich" INTEGER,
	Savings_rich INTEGER,
	Check_moderate INTEGER,
	Check_no_inf INTEGER,
	Check_rich INTEGER,
	credit_amount REAL,
	duration INTEGER,
	Purpose_car INTEGER,
	"Purpose_domestic appliances" INTEGER,
	Purpose_education INTEGER,
	"Purpose_furniture/equipment" INTEGER,
	"Purpose_radio/TV" INTEGER,
	Purpose_repairs INTEGER,
	"Purpose_vacation/others" INTEGER
);



CREATE TABLE IF NOT EXISTS tbl_results(
    pk_id INTEGER,
    transaction_id VARCHAR(50),
    model VARCHAR(20),
	"Risk" VARCHAR(7),
    age INTEGER,
	sex VARCHAR(50),
	job INTEGER,
	housing VARCHAR(50),
	saving_accounts VARCHAR(50),
	checking_account VARCHAR(50),
	credit_amount INTEGER,
	duration INTEGER,
	purpose VARCHAR(50),
	UNIQUE(pk_id, model)
); --UNIQUE pk_id, model



CREATE TABLE IF NOT EXISTS tbl_cat_age (
	"Value" TEXT,
  	"Age_cat_Young" INTEGER,
  	"Age_cat_Adult" INTEGER,
  	"Age_cat_Senior" INTEGER
);



CREATE TABLE IF NOT EXISTS tbl_cat_checking (
	"Value" TEXT,
  	"Check_moderate" INTEGER,
  	"Check_no_inf" INTEGER,
  	"Check_rich" INTEGER
);


CREATE TABLE IF NOT EXISTS tbl_cat_housing (
	"Value" TEXT,
  	"Housing_own" INTEGER,
  	"Housing_rent" INTEGER
);


CREATE TABLE IF NOT EXISTS tbl_cat_purpose (
	"Value" TEXT,
  	"Purpose_car" INTEGER,
  	"Purpose_domestic appliances" INTEGER,
  	"Purpose_education" INTEGER,
  	"Purpose_furniture/equipment" INTEGER,
  	"Purpose_radio/TV" INTEGER,
  	"Purpose_repairs" INTEGER,
  	"Purpose_vacation/others" INTEGER
);


CREATE TABLE IF NOT EXISTS tbl_cat_savings (
	"Value" TEXT,
  	"Savings_moderate" INTEGER,
  	"Savings_no_inf" INTEGER,
  	"Savings_quite rich" INTEGER,
  	"Savings_rich" INTEGER
);


CREATE TABLE IF NOT EXISTS tbl_cat_sex (
	"Value" TEXT,
  	"Sex_male" INTEGER
);










