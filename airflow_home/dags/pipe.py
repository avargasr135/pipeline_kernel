from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python import PythonOperator

import sys
import os
parent = os.path.abspath('.')
sys.path.append(parent)

import uuid

from raw_insert_to_database import insert_raw_data_by_chunks
from insert_encoding_values import encoding_and_insert_data
from make_and_insert_prediction import insert_predictions
from write_prediction_parquet import write_results_to_parquets


# Genera un identificador único UUID que servirá para hacer
#track de la transacción en curso, y será accedida por una variable XCOM
def generate_uuid(ti = None):
    uuid_generated = str(uuid.uuid4())
    ti.xcom_push(key = "uuid", value = uuid_generated)


def_args = {"owner": "Alberto",
            "depends_on_past": False,
            "start_date": datetime(2023, 3, 23),
            "email": ["avargasr135@gmail.com"],
            "email_on_failure": True,
            "retries": 1,
            "retry_delay": timedelta(seconds=10)
            }


with DAG(dag_id="Kernel_pipeline",
         description= "Flujo de trabajo para hacer..",
         schedule= timedelta(days=1),
         start_date=datetime(2023, 3, 23),
         default_args = def_args,
         catchup= False) as dag:

    generate_uuid_task = PythonOperator(task_id='generate_uuid',
                                   python_callable = generate_uuid)

    extract_and_insert_raw = PythonOperator(task_id = 'insert_input_data',
                                 python_callable = insert_raw_data_by_chunks,
                                 op_args = ['{{ts}}'],
                                 dag = dag)

    insert_encoding_data = PythonOperator(task_id = 'insert_encoding_data',
                                 python_callable = encoding_and_insert_data,
                                 dag = dag)

    pred_random_forest = PythonOperator(task_id = 'model_prediction_random_forest',
                                 python_callable = insert_predictions,
                                 op_args = ['random forest'],
                                 dag = dag)

    pred_gaussian = PythonOperator(task_id = 'model_prediction_gaussian',
                                 python_callable = insert_predictions,
                                 op_args = ['gaussian model'],
                                 dag = dag)

    pred_xboost = PythonOperator(task_id = 'model_prediction_xboost',
                                 python_callable = insert_predictions,
                                 op_args = ['xboost model'],
                                 dag = dag)

    write_random_forest = PythonOperator(task_id = 'results_to_parquet_random_forest_model',
                                 python_callable = write_results_to_parquets,
                                 op_args = ['random forest'],
                                 dag = dag)

    write_gaussian = PythonOperator(task_id = 'results_to_parquet_gaussian_model_model',
                                 python_callable = write_results_to_parquets,
                                 op_args = ['gaussian model'],
                                 dag = dag)

    write_xboost = PythonOperator(task_id = 'results_to_parquet_xboost_model_model',
                                 python_callable = write_results_to_parquets,
                                 op_args = ['xboost model'],
                                 dag = dag)


    generate_uuid_task >> extract_and_insert_raw >> insert_encoding_data
    insert_encoding_data >> [pred_random_forest, pred_gaussian, pred_xboost]
    pred_random_forest >> write_random_forest
    pred_gaussian >> write_gaussian
    pred_xboost >> write_xboost




















