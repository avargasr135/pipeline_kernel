
import pandas as pd
import numpy as np

import sqlite3
sqlite3.register_adapter(np.int64, lambda val: int(val))
sqlite3.register_adapter(np.int32, lambda val: int(val))

from const import PATH_INPUT_DATA_CSV, DATABASE, INPUT_TABLE_NAME


def csv_reader(filename_path):
    _CHUNKSIZE = 10 ** 6
    return pd.read_csv(filename_path, usecols=list(range(1,10)), chunksize = _CHUNKSIZE)


def insert_to_input_table(data_frame):
    records = list(data_frame.to_records(index=False))

    insert_statment = f'''INSERT INTO {INPUT_TABLE_NAME}
    (transaction_id, rundate, age, sex, job, housing, saving_accounts, checking_account,
    credit_amount, duration, purpose) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''

    with sqlite3.connect(DATABASE) as conn:
        cursor = conn.cursor()
        cursor.executemany(insert_statment, records)
        conn.commit()


def insert_raw_data_by_chunks(time_execution, ti = None):
    transaction_id = ti.xcom_pull(key="uuid", task_ids="generate_uuid")
    rundate = time_execution
    print("ID = ", transaction_id)

    original_columns = []
    for df_chunk in csv_reader(PATH_INPUT_DATA_CSV):
        print("---- SHAPE ", df_chunk.shape)
        if not original_columns:
            original_columns = list(df_chunk.columns)
            new_columns = ["transaction_id", "rundate"] + original_columns

        df_chunk["transaction_id"] = transaction_id
        df_chunk["rundate"] = rundate
        df_chunk = df_chunk[new_columns]

        insert_to_input_table(df_chunk)








