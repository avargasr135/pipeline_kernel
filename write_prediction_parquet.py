import os

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

import sqlite3

from const import DATABASE, FINAL_TABLE, DIR_OUTPUT_DATA


def chunks_results_data(uuid, model_name):
    _CHUNKSIZE = 10 ** 6

    SELECT_QUERY = f'''
                    SELECT pk_id, "Risk", age, sex, job, housing, saving_accounts,
                    checking_account, credit_amount, duration, purpose
                    FROM {FINAL_TABLE}
                    WHERE transaction_id = '{uuid}' AND model = '{model_name}';
                    '''

    with sqlite3.connect(DATABASE) as conn:
        for df_chunk in pd.read_sql(SELECT_QUERY, conn, chunksize = _CHUNKSIZE):
            yield df_chunk


def write_results_to_parquets(model_name, ti = None, transaction_id = None):
    if ti:
        uuid = ti.xcom_pull(key="uuid", task_ids="generate_uuid")
    else: # For testing
        uuid = transaction_id

    _FINAL_DIR = os.path.join(DIR_OUTPUT_DATA, uuid, model_name)
    if not os.path.exists(_FINAL_DIR):
        os.makedirs(_FINAL_DIR)
    output_path_file = os.path.join(_FINAL_DIR, "output.parquet")

    parquet_writer = None
    for df_chunk in chunks_results_data(uuid, model_name):
        table = pa.Table.from_pandas(df_chunk)

        if not parquet_writer:
            parquet_writer = pq.ParquetWriter(output_path_file, table.schema)

        parquet_writer.write_table(table)
    parquet_writer.close()



if __name__ == '__main__':
    write_results_to_parquets(model_name="xboost model", transaction_id = "8b87ce1e-f3b0-4d77-8e59-6fb8c8216afe")
