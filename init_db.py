import sqlite3
import pandas as pd

from const import DATABASE, PATH_INPUT_DATA_CSV


def execute_sql_script(filename, db = DATABASE):
    with open(filename, 'r') as sql_file:
        sql_script = sql_file.read()

    with sqlite3.connect(db) as conn:
        cursor = conn.cursor()
        cursor.executescript(sql_script)
        conn.commit()


def fill_catalogs():
    def __transform_data(dataframe):
        interval = (18, 25, 35, 60, 120)

        cats = ['Student', 'Young', 'Adult', 'Senior']
        dataframe['Age_cat'] = pd.cut(dataframe.Age, interval, labels=cats)

        dataframe['Saving accounts'] = dataframe['Saving accounts'].fillna('no_inf')
        dataframe['Checking account'] = dataframe['Checking account'].fillna('no_inf')


    def __get_catalog(data_credit, column_name, col_prefix):
        unique_values = data_credit[column_name].unique()
        catalog = pd.get_dummies(unique_values, drop_first=True, prefix=col_prefix)
        catalog["Value"] = unique_values
        col = list(catalog.columns)

        return catalog[[col[-1]]+col[:-1]] # Cambio de posición las columnas

    df_credit = pd.read_csv(PATH_INPUT_DATA_CSV, usecols=list(range(1,10)))

    __transform_data(df_credit)

    cat_purpose = __get_catalog(df_credit, 'Purpose', 'Purpose')
    cat_sex = __get_catalog(df_credit, 'Sex', 'Sex')
    cat_housing = __get_catalog(df_credit, 'Housing', 'Housing')
    cat_savings = __get_catalog(df_credit, 'Saving accounts', 'Savings')
    #cat_risk = __get_catalog(df_credit, 'Risk', 'Risk') #TARGET
    cat_checking = __get_catalog(df_credit, 'Checking account', 'Check')
    cat_age = __get_catalog(df_credit, 'Age_cat', 'Age_cat')

    with sqlite3.connect(DATABASE) as conn:
        cat_purpose.to_sql('tbl_cat_purpose', con=conn, if_exists='replace', index=False)
        cat_sex.to_sql('tbl_cat_sex', con=conn, if_exists='replace', index=False)
        cat_housing.to_sql('tbl_cat_housing', con=conn, if_exists='replace', index=False)
        cat_savings.to_sql('tbl_cat_savings', con=conn, if_exists='replace', index=False)
        cat_checking.to_sql('tbl_cat_checking', con=conn, if_exists='replace', index=False)
        cat_age.to_sql('tbl_cat_age', con=conn, if_exists='replace', index=False)




if __name__ == "__main__":
    filename = "./schemas.sql"

    execute_sql_script(filename)

    fill_catalogs()
    print("Done !!! ")