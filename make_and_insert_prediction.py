import pandas as pd
import numpy as np

import pickle

import sqlite3
sqlite3.register_adapter(np.int64, lambda val: int(val))
sqlite3.register_adapter(np.int32, lambda val: int(val))

from const import (DATABASE, ENCODING_TABLE_NAME, INPUT_TABLE_NAME, FINAL_TABLE,
                   RANDOM_FOREST_MODEL, GAUSSIAN_MODEL, XBOOST_MODEL)


def load_pickle_model(path_to_model):
    model = pickle.load(open(path_to_model, 'rb'))
    return model


def chunks_encoding_data(uuid):
    _CHUNKSIZE = 10 ** 6

    SELECT_QUERY = f'''
    SELECT pk_id, transaction_id, age, job, credit_amount, duration, "Purpose_car", "Purpose_domestic appliances",
    "Purpose_education", "Purpose_furniture/equipment", "Purpose_radio/TV", "Purpose_repairs",
    "Purpose_vacation/others", "Sex_male", "Housing_own", "Housing_rent", "Savings_moderate",
    "Savings_no_inf", "Savings_quite rich", "Savings_rich", "Check_moderate", "Check_no_inf",
    "Check_rich", "Age_cat_Young", "Age_cat_Adult", "Age_cat_Senior"
    FROM {ENCODING_TABLE_NAME} WHERE transaction_id = '{uuid}'
    '''
    with sqlite3.connect(DATABASE) as conn:
        for df_chunk in pd.read_sql(SELECT_QUERY, conn, chunksize = _CHUNKSIZE):
            yield df_chunk


## bad -> 1 good -> 0
def insert_predict_chunk_to_results(data_chunk, pkl_model, model_name):
    _columns_to_insert = ["pk_id", "transaction_id", "model", "Risk"]
    _mapping = {0 : "good", 1 : "bad"}

    data_chunk["Risk"] = pkl_model.predict(data_chunk.iloc[:, 2:].values)
    data_chunk["Risk"] = data_chunk["Risk"].map(_mapping)

    data_chunk["model"] = model_name

    records = list(data_chunk[_columns_to_insert].to_records(index=False))

    insert_statment = f'''INSERT INTO {FINAL_TABLE}
    (pk_id, transaction_id, model, 'Risk') VALUES (?, ?, ?, ?)'''

    with sqlite3.connect(DATABASE) as conn:
        cursor = conn.cursor()
        cursor.executemany(insert_statment, records)
        conn.commit()


## Fills the other columns : age, sex, ...
def complete_tbl_results(transaction_id, model_name):
    UPDATE_QUERY = f'''
            WITH complete_data AS (
            SELECT
                A.pk_id, A.transaction_id, A.model, A.'Risk',
                B.age, B.sex, B.job, B.housing, B.saving_accounts,
                B.checking_account, B.credit_amount, B.duration,
                B.purpose
                FROM {FINAL_TABLE} A
                LEFT JOIN {INPUT_TABLE_NAME} B
                    ON A.pk_id = B.pk_id AND A.transaction_id = B.transaction_id
                WHERE A.transaction_id = '{transaction_id}' AND A.model = '{model_name}'
            )
            INSERT INTO {FINAL_TABLE}
            SELECT * FROM complete_data WHERE TRUE
            ON CONFLICT(pk_id, model) DO UPDATE
                SET age = excluded.age,
                    sex = excluded.sex,
                    job = excluded.job,
                    housing = excluded.housing,
                    saving_accounts = excluded.saving_accounts,
                    checking_account = excluded.checking_account,
                    credit_amount = excluded.credit_amount,
                    duration = excluded.duration,
                    purpose = excluded.purpose;
    '''

    with sqlite3.connect(DATABASE) as conn:
        cursor = conn.cursor()
        cursor.execute(UPDATE_QUERY)
        conn.commit()


def insert_predictions(model_name, ti = None, uuid = None):
    uuid = ti.xcom_pull(key="uuid", task_ids="generate_uuid")

    if model_name == "random forest":
        model = load_pickle_model(RANDOM_FOREST_MODEL)
    elif model_name == "gaussian model":
        model = load_pickle_model(GAUSSIAN_MODEL)
    elif model_name == "xboost model":
        model = load_pickle_model(XBOOST_MODEL)
    else:
        SystemError

    for data_chunk in chunks_encoding_data(uuid):
        insert_predict_chunk_to_results(data_chunk, model, model_name)

    #finally
    complete_tbl_results(uuid, model_name)


if __name__ == "__main__":
    uuid = 'adcc9f6e-b8db-4012-ba7b-e889e36f2057'
    model_name = "xboost model"
    insert_predictions(model_name, uuid = uuid)

