from pathlib import Path
import os
import sys
parent = os.path.abspath('.')
sys.path.append(parent)

#Load the librarys
import pandas as pd #To work with dataset
import numpy as np #Math library
import pickle

from sklearn.model_selection import train_test_split # to split the data
from sklearn.metrics import accuracy_score

from const import RANDOM_FOREST_MODEL, GAUSSIAN_MODEL, XBOOST_MODEL

HOME_DIR = Path(__file__).resolve().parent
DIR_TRAIN_DATA = os.path.join(HOME_DIR, "train_data")
TRAIN_DATA = os.path.join(DIR_TRAIN_DATA, "german_credit_data_with_target.csv")


def read_train_data():
    return pd.read_csv(TRAIN_DATA, index_col=0)


def get_age_categories(df_credit):
    #Let's look the Credit Amount column
    interval = (18, 25, 35, 60, 120)

    cats = ['Student', 'Young', 'Adult', 'Senior']
    df_credit["Age_cat"] = pd.cut(df_credit.Age, interval, labels=cats)


def print_insigths(df_credit):
    print("Purpose : ",df_credit.Purpose.unique())
    print("Sex : ",df_credit.Sex.unique())
    print("Housing : ",df_credit.Housing.unique())
    print("Saving accounts : ",df_credit['Saving accounts'].unique())
    print("Risk : ",df_credit['Risk'].unique())
    print("Checking account : ",df_credit['Checking account'].unique())
    print("Aget_cat : ",df_credit['Age_cat'].unique())


def feature_engineering(df_credit):
    df_credit['Saving accounts'] = df_credit['Saving accounts'].fillna('no_inf')
    df_credit['Checking account'] = df_credit['Checking account'].fillna('no_inf')

    #Purpose to Dummies Variable
    df_credit = df_credit.merge(pd.get_dummies(df_credit.Purpose, drop_first=True, prefix='Purpose'), left_index=True, right_index=True)
    #Sex feature in dummies
    df_credit = df_credit.merge(pd.get_dummies(df_credit.Sex, drop_first=True, prefix='Sex'), left_index=True, right_index=True)
    # Housing get dummies
    df_credit = df_credit.merge(pd.get_dummies(df_credit.Housing, drop_first=True, prefix='Housing'), left_index=True, right_index=True)
    # Housing get Saving Accounts
    df_credit = df_credit.merge(pd.get_dummies(df_credit["Saving accounts"], drop_first=True, prefix='Savings'), left_index=True, right_index=True)
    # Housing get Risk
    df_credit = df_credit.merge(pd.get_dummies(df_credit.Risk, prefix='Risk'), left_index=True, right_index=True)
    # Housing get Checking Account
    df_credit = df_credit.merge(pd.get_dummies(df_credit["Checking account"], drop_first=True, prefix='Check'), left_index=True, right_index=True)
    # Housing get Age categorical
    df_credit = df_credit.merge(pd.get_dummies(df_credit["Age_cat"], drop_first=True, prefix='Age_cat'), left_index=True, right_index=True)

    #Excluding the missing columns
    del df_credit["Saving accounts"]
    del df_credit["Checking account"]
    del df_credit["Purpose"]
    del df_credit["Sex"]
    del df_credit["Housing"]
    del df_credit["Age_cat"]
    del df_credit["Risk"]
    del df_credit['Risk_good']

    df_credit['Credit amount'] = np.log(df_credit['Credit amount'])

    return df_credit


def split_data(df_credit):
    #Creating the X and y variables
    X = df_credit.drop('Risk_bad', 1).values
    y = df_credit["Risk_bad"].values

    # Spliting X and y into train and test version
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state=42)

    return X_train, X_test, y_train, y_test


def get_random_forest_model(X_train, y_train):
    from sklearn.ensemble import RandomForestClassifier
    rf = RandomForestClassifier(max_depth=None, max_features=10, n_estimators=15, random_state=2)

    #trainning with the best params
    rf.fit(X_train, y_train)
    return rf


def get_gaussian_naive_bayes(X_train, y_train):
    from sklearn.naive_bayes import GaussianNB
    GNB = GaussianNB()

    # Fitting with train data
    model = GNB.fit(X_train, y_train)
    return model


def get_xboost(X_train, y_train):
    from xgboost import XGBClassifier
    from sklearn.model_selection import GridSearchCV

    #Creating the classifier
    model_xg = XGBClassifier(random_state=2, colsample_bytree = 0.75, gamma = 0.2,
                             max_depth = 5, min_child_weight = 3, subsample = 0.8)
    model = model_xg.fit(X_train, y_train)

    return model


def saving_to_pickle(model, filename):
    pickle.dump(model, open(filename, 'wb'))


def main():
    training_data = read_train_data()

    get_age_categories(training_data)

    print_insigths(training_data)

    training_data = feature_engineering(training_data)

    training_data.info()

    X_train, X_test, y_train, y_test = split_data(training_data)

    #### TRAINING ----
    random_forest_model = get_random_forest_model(X_train, y_train)
    gaussian_model = get_gaussian_naive_bayes(X_train, y_train)
    xboost_model = get_xboost(X_train, y_train)


    y_pred = random_forest_model.predict(X_test)
    print("Accuray score random forest model")
    print(accuracy_score(y_test,y_pred))

    y_pred = gaussian_model.predict(X_test)
    print("Accuray score gaussian model")
    print(accuracy_score(y_test,y_pred))

    y_pred = xboost_model.predict(X_test)
    print("Accuray score xboost model")
    print(accuracy_score(y_test,y_pred))


    ### SAVING MODELS ---
    saving_to_pickle(random_forest_model, RANDOM_FOREST_MODEL)
    saving_to_pickle(gaussian_model, GAUSSIAN_MODEL)
    saving_to_pickle(xboost_model, XBOOST_MODEL)


if __name__ == "__main__":
    main()
    print("Done!!!")




