
# Pipeline de modelo de riesgo

Se usó los pasos que describe el [kernel](https://www.kaggle.com/code/kabure/predicting-credit-risk-model-pipeline/notebook) de kaggel para el desarrollo del pipe:
https://www.kaggle.com/code/kabure/predicting-credit-risk-model-pipeline/notebook



Modo de incializar el modelo:

 - Se clona el repositorio `git clone https://gitlab.com/avargasr135/...`
 - Una vez clonado abrir una terminal en el directorio de la carpeta donde se clonó el repo, y lo que hay que hacer es crear un entorno virtual de python, y activarlo, para instalar las dependencias:
    ```bash
        python3 -m venv .venv

        source .venv/bin/activate

        pip install -r requirements.txt
    ```

 - El siguiente paso sería inicializar la base de datos sqlite donde se depositará las información que produce el pipeline.
    ```bash
        python3 init_db.py
    ```
    Esto crea el archivo `my_test_database.db`, y sobre éste define los esquemas de tablas a usar.

 - Ahora se definen la variable de entorno en consola del *home* para airflow como:

    ```bash
        export AIRFLOW_HOME=${PWD}/airflow_home
    ```

 - Ejecutamos en la misma terminal

    ```bash
        airflow db init
    ```

    Para iniciar la base de datos airflow. Se agrega al usuario de administración
    ```bash
        airflow users create --username admin --firstname Mi_nombre --lastname Mi_apellido --role Admin --email mail@domain.com
    ```
    En este punto solicitará proporcionar una contraseña para el administrador.

 - Se incial el servidor de airflow y el scheduler.
 Usando la misma terminal, se escribe:

    ```bash
        airflow webserver -p 8080
    ```
    Y en una nueva terminal (habilitanto el ambiente virtual y colocando la variable de entorno AIRFLOW_HOME como antes), se inicia el scheduler:

    ```bash
        airflow scheduler
    ```
 - Una vez completados los pasos en localhost:8080 se debe mostrar la interfaz de inicio donde se pone al usuario admin, con la contraseña proporcionada.

 - El nombre del pipeline, es decir el *dag_id* es `Kernel_pipeline`. La definición se encuentra ubicada en `./airflow_home/dags/pipe.py`

Mi solución propuesta se muestra en el siguiente diagrama:

![image info](Test-Diagrama.drawio.png "Diagrama origen destino")

Ahora, se intentará describir en grandes rasgos los pasos que se colocan aquí:

- Se asume que toda la información de entrada, es decir de los casos que se pretenden predecir pertenecen a un archivo de tipo csv colocados en `./input_data/german_credit_data.csv`.
La lectura de este archivo se hace por medio de la librería de pandas de python, y se hace por ***batches***, donde se ha configurado que estos *batches* comprendan <ins>1 millon</ins> de filas para insertarlas y no produzca un desbordamiento en la memoria.

- La inserción pasa a la base de datos sqlite, sobre la tabla de nombre `tbl_raw_input_data`.

- En la etapa del *feature engineering* que propone el kernel, toda la operación se le deja a la base de datos, y con python simplemente se ejecuta en una conexión. En esta parte se codifican las columnas que son de categoría, así como también se obtiene la cateoría de las edades, y se escala con el logaritmo natural a la columna *credit_amount*. La codificación se apoya de las tablas catálogo que se generan al incializar la base de datos, que resulta de hacer un join entre los datos de entrada con los catálogos.
La información codificada se inserta sobre la tabla `tbl_encoding_data`.

- Teniendo la información codificada, ya se puede aplicar el modelo, o los modelos en este caso (encontré que dentro del kernel prepara 3 modelos *bosques aleatorios, modelo gaussian naivy bayes, y xboost*).
La estrategía a proceder fue tener los modelos ya pre-entrenados y guardados en un archivo binario *pickle*, donde simplemente puedan ser llamados y realizar las predicciones. El entrenamiento aunque no es parte del pipeline lo coloco en el archivo `./models/training.py`, los modelos se entrenan (siguiendo los pasos del kernel) con la información de `./models/train_data/german_credit_data_with_target.csv`, y generan los 3 modelos en formato pickle.
Para las predicciones se cargan estos modelos, y con la información leída de *tbl_encoding_data* filtrada por el `transaction_id` del proceso. La lectura de igual manera se realiza por medio de ***batches*** de filas compuestas por <ins>1 millón</ins>, pues se realizan con pandas y para evitar la carga a la memoria, se realizan las predicciones por trozos, y conforme se van liberando se escriben en la tabla `tbl_results`.

- Por último se lleva a cabo el proceso de escribir los resultados a un archivo parquet, de la transacción en curso, leyendo de igual manera por batches de la base de datos, filtrando por `transaction_id`, y escribiendo el archivo final en `./parquet_outputs/{transaction_id}/{model_name}/output.parquet`.




















