import sqlite3

from const import DATABASE, INPUT_TABLE_NAME, ENCODING_TABLE_NAME


def encoding_and_insert_data(ti = None, transaction_id = None):
    if ti:
        id = ti.xcom_pull(key="uuid", task_ids="generate_uuid")
    else: # For testing
        id = transaction_id
    print("id = ", id)

    QUERY = f'''
        WITH input_data AS (
        SELECT pk_id, transaction_id, age,
        CASE
            WHEN 18 < age AND age <= 25 THEN 'Student'
            WHEN 25 < age AND age <= 35 THEN 'Young'
            WHEN 35 < age AND age <= 60 THEN 'Adult'
            WHEN 60 < age AND age <= 120 THEN 'Senior' END AS 'Age_cat',
        sex, job, housing, COALESCE(saving_accounts, 'no_inf') AS saving_accounts,
        COALESCE(checking_account, 'no_inf') AS checking_account, LN(credit_amount) AS credit_amount,
        duration, purpose
        FROM '{INPUT_TABLE_NAME}'
            WHERE transaction_id = '{id}'
        ), encoding AS (
        SELECT
            pk_id, transaction_id, age,
            B.Age_cat_Young, B.Age_cat_Adult, B.Age_cat_Senior,
            C.Sex_male,
            A.job,
            D.Housing_own, D.Housing_rent,
            E.Savings_moderate, E.Savings_no_inf, E."Savings_quite rich", E.Savings_rich,
            F.Check_moderate, F.Check_no_inf, F.Check_rich,
            A.credit_amount, A.duration,
            G.Purpose_car, G."Purpose_domestic appliances", G.Purpose_education,
            G."Purpose_furniture/equipment", G."Purpose_radio/TV", G.Purpose_repairs,
            G."Purpose_vacation/others"
            FROM input_data A
                LEFT JOIN tbl_cat_age B
                    ON A.'Age_cat' = B.Value
                LEFT JOIN tbl_cat_sex C
                    ON A.sex = C.Value
                LEFT JOIN  tbl_cat_housing D
                    ON A.housing = D.Value
                LEFT JOIN tbl_cat_savings E
                    ON A.saving_accounts = E.Value
                LEFT JOIN tbl_cat_checking F
                    ON A.checking_account = F.Value
                LEFT JOIN tbl_cat_purpose G
                    ON A.purpose = G.Value
        )
        INSERT INTO '{ENCODING_TABLE_NAME}'
        SELECT *
        FROM encoding;
    '''

    with sqlite3.connect(DATABASE) as conn:
        cursor = conn.cursor()
        cursor.execute(QUERY)
        conn.commit()


if __name__ == "__main__":
    id = '057d1f44-c9ac-11ed-89ae-e880883fcf60'
    encoding_and_insert_data(id)
