from pathlib import Path
import os


#### DATABASE CONSTANTS ####

DATABASE = 'my_test_database.db'
INPUT_TABLE_NAME = 'tbl_raw_input_data'
ENCODING_TABLE_NAME = 'tbl_encoding_data'
FINAL_TABLE = 'tbl_results'



#### PATH CONSTANTS ####

__HOME_DIR = Path(__file__).resolve().parent
DIR_INPUT_DATA = os.path.join(__HOME_DIR, "input_data")
PATH_INPUT_DATA_CSV = os.path.join(DIR_INPUT_DATA, "german_credit_data.csv")

DIR_OUTPUT_DATA = os.path.join(__HOME_DIR, "parquet_outputs")
if not os.path.exists(DIR_OUTPUT_DATA):
    os.makedirs(DIR_OUTPUT_DATA)



#### MODELS LOCATIONS

DIR_MODELS = os.path.join(__HOME_DIR, "models")
RANDOM_FOREST_MODEL = os.path.join(DIR_MODELS, "random_forest_model.pickle")
GAUSSIAN_MODEL = os.path.join(DIR_MODELS, "gaussian_model.pickle")
XBOOST_MODEL = os.path.join(DIR_MODELS, "xboost_model.pickle")